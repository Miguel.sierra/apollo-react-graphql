Npm Server
# Move to server folder
$ cd server

# Install dependencies
$ npm install

# Load data
$ npm run seed

# Run server
$ npm run start:slow

Npm client
cd client && npm start
npm i -S graphql apollo-boost react-apollo